import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_KEY = 'AIzaSyC72kSpizEs3bySYil7wMJj1k651yG3FoY';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = { 
			videos: [],
			selectedVideo: null 
		}
		this.videoSearchTerm('devops');
		
	}
	videoSearchTerm(term) {
			YTSearch({key: API_KEY, term: term}, (videos) => {
				this.setState({ 
					videos,
					selectedVideo: videos[0] 
				})
			});
		}
	render() {
		return (
			<div className="container-fluid" >
				<div className='row justify-content-md-center'>
					<div className="col-md-offset-4 col-md-8" >
						< SearchBar onSearchTermChanged = {term => this.videoSearchTerm(term)}/>
					</div>
				</div>
				<div className="row justify-content-md-center" >
					<div className=" col-md-7 " >
						< VideoDetail video= { this.state.selectedVideo } />
					</div>
					<div className="col-md-4" >
						< VideoList 
							videos = { this.state.videos } 
							onVideoSelect = { selectedVideo => this.setState({ selectedVideo })}
						/>
					</div>
				</div>
				
			</div>
			)
	}
}

ReactDOM.render(<App />, document.querySelector('#root'));