import React from 'react';

const VideoDetail = ({video}) => {

	if( !video ) {
		return <div>Loading..</div>;
	}

	const title = video.snippet.title
	const description = video.snippet.description;
	const videoId = video.id.videoId;
	const videoURL = `https://www.youtube.com/embed/${videoId}`;

	return(
		<div className="video-detail">
			<div className="embed-responsive embed-responsive-16by9">
				<iframe className="embed-responsive-item" src={ videoURL } ></iframe>
			</div>

			<div className="details">
				<h5> { title } </h5>
				<p> { description } </p>
			</div>
		</div>
	);
}

export default VideoDetail;